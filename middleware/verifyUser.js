const { users } = require("../data/users");

const verifyUser = (req, res, next) => {
  let auth = req.headers['authorization'];
  let user = users.find(u => u.token === auth);
  if (user) {
    res.user = user;
    next();
  } else {
    res.status(401).json({
      message: "unauthorized"
    });
  }
};

module.exports = { verifyUser };
