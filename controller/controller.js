const { users } = require("../data/users");

const homePage = (req, res) => {
    res.status(200).render('index');
};

const gamePage = (req, res) => {
    res.status(200).render('game');
};

const loginUser = (req, res) => {
    let body = req.body;
    let email = body.email;
    let password = body.password;
    
    let user = users.find(u => u.email === email);

    if (user && user.password === password){
        res.status(200).send({
            message : "Login Berhasil",
            token : user.token
        });
    } else {
        res.status(401).send({
            message : "Login Gagal",
        });
    };
};

const profileUser = (req, res) => {
    res.status(200).send({
        email : res.user.email,
        phoneNumber: res.user.phoneNumber, 
        location: res.user.location
    });
}

module.exports = { homePage, gamePage, loginUser, profileUser };