# Challange Chapter 5

### About This Project :

- Making Simple Fullstack app
- Combine Challange Chapter 3 & 4 into this project
- Learning express
- Learning HTTP
- Learning Testing API using Postman
- Learning RESTful API
- Practice to serving static file and JSON
- Practice to build controller
- Practice to build middleware
