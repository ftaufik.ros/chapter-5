const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");

const { homePage, gamePage, loginUser, profileUser } = require("./controller/controller");
const { verifyUser } = require("./middleware/verifyUser");

const port = 3000;
const jsonParser = bodyParser.json();


app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));


app.get('/', jsonParser, homePage);

app.get('/game', jsonParser, gamePage);

app.post('/login', jsonParser, loginUser);

app.post('/profile', jsonParser, verifyUser, profileUser);



app.listen(port, () => console.log("Server running at http://localhost:"+port));