class Game {

    getComputerChoice() {
        const randomNumber = Math.floor(Math.random() * 3 + 1);
        
        if (randomNumber === 1) {
            return 'rock';
        } 

        else if (randomNumber === 2) {
            return 'paper';
        } 

        else {
            return 'scissor';
        };
    }

    getWinner(p, c) {
        this.player = p;
        this.com = c;

        if (p === c) {
            return 'Draw';
        }
        
        else if (p === 'scissor' && c === 'paper' || p === 'paper' && c === 'rock' || p === 'rock' && c === 'scissor' ) {
            return 'Player 1 Win';
        }  
        
        else {
            return 'Com Win';
        };
    }
}

const newGame = new Game();

const userChoices = document.querySelectorAll('.user-select'); //[0,1,2]
const botChoices = document.querySelectorAll('.com-select'); //[0,1,2]
const revealWinner = document.getElementById('result');
const refresher = document.getElementById('refresh');


userChoices.forEach(choice => choice.addEventListener('click', e => {
    const playerChoice = e.target.id;
    const comChoice = newGame.getComputerChoice();
    winner = newGame.getWinner(playerChoice, comChoice);

    const revealPlayerChoice = document.getElementById(playerChoice);
    const revealComChoice = document.getElementById(`com-${comChoice}`);
    
    // menampilkan pemenang
    if (winner === "Draw" ){
        revealWinner.innerText = `${winner}`;
        revealWinner.classList.add('draw');
        revealWinner.classList.remove('vs-text');
    } else {
        revealWinner.innerText = `${winner}`;
        revealWinner.classList.add('the-winner');
        revealWinner.classList.remove('vs-text'); 
    }

    // menampilkan hasil pilihan
    revealPlayerChoice.classList.add('btn-select');
    revealComChoice.classList.add('btn-select');
    
    // disable button pilihan player
    userChoices[0].setAttribute("disabled", "");
    userChoices[1].setAttribute("disabled", "");
    userChoices[2].setAttribute("disabled", "");
}));


refresher.addEventListener('click', e => {
    // hidden button hasil pilhan user
    userChoices[0].classList.remove('btn-select');
    userChoices[1].classList.remove('btn-select');
    userChoices[2].classList.remove('btn-select');
    
    // hidden button hasil pilhan computer
    botChoices[0].classList.remove('btn-select');
    botChoices[1].classList.remove('btn-select');
    botChoices[2].classList.remove('btn-select');
    
    // enable button pilhan
    userChoices[0].removeAttribute("disabled");
    userChoices[1].removeAttribute("disabled");
    userChoices[2].removeAttribute("disabled");

    // remove style pemenang
    revealWinner.innerText = "VS";
    revealWinner.classList.add('vs-text');
    revealWinner.classList.remove('the-winner');
    revealWinner.classList.remove('draw');
});


