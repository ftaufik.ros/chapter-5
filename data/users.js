let users = [
  { email: "tes1@mail.com", password: "tes1", token: "a-b-c", phoneNumber: "+62812xxx", location: "Indonesia" },
  { email: "tes2@mail.com", password: "tes2", token: "d-e-f", phoneNumber: "+60552xxx", location: "Malaysia" }
];

module.exports = { users };
